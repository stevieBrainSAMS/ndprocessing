%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Project setup                                     %                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add required toolboxes
addpath(genpath('C:\toolboxChanges\depomod-matlab-toolbox'));
addpath(genpath('C:\Matlab_Toolbox\os_toolbox-master'));
addpath(genpath('C:\Matlab_Toolbox\rcm_toolbox-master'));

%% Read existing project
% name='TestPROJECT';
% projectDir = ['C:\DepomodProjects\' name];
% project=NewDepomod.Project.create(projectDir);
% inputsDir=[projectDir '\depomod\inputs\'];
%% Create project from template
name='TestPROJECT';
rootDir = 'C:\DepomodProjects\';
project = NewDepomod.Project.createFromTemplate(rootDir, name);
projectDir = ['C:\DepomodProjects\' name];
inputsDir=[projectDir '\depomod\inputs\'];
%% Choose cagelayout&chem to run
run=project.solidsRuns.first;

%% Check files for consistency and that SEPA required defaults are set
checkConsist(project,run)

% must reload project and run after making changes before checking again
%% Execute without any change
%in cmd line
run.execute
%% in matlab
run.execute('runInBackground', 0)
%%                                                                       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Create runs, change inputs and parameters                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create new run from old
%code ensures input files for each cage group are created

label='1'; %run to copy
run=project.solidsRuns.label(label);
g=run.cages.size; %number of CageGroups
cpR=1; %number of copy runs to make 
for runN=1:cpR 
    newlabel=[label '_' num2str(runN)];
    run=project.solidsRuns.new('label',newlabel,'template',label);
    cd (inputsDir);
    
for m=1:g
    k=num2str(m);
    p=[name '-' label '-NONE-CageGroup' k '.depomodinputsproperties'];
    q=[name '-' newlabel '-NONE-CageGroup' k '.depomodinputsproperties'];
    copyfile(p, q)
end
end

%% Change biomass, length of run, or other parameters
label='1_1'; %label of run to change
run=project.solidsRuns.label(label);
g=run.cages.size;
inputs=run.inputsFile;
biomass=800; %new biomass
inputs.setBiomass(biomass)
% inputs.setBiomass(biomass, 'days',365, 'feedRatio',7, ...
%     'feedWaterPercentage',9, 'feedWastePercentage',3, ...
%     'feedAbsorbedPercentage',85, 'feedCarbonPercentage',49, ...
%     'faecesCarbonPercentage',30)
inputs.toFile

cd(inputsDir)
inputs.data=inputs.data/g;
inputs.FeedInputs.biomass=num2str(biomass/g);
for m=1:g
    k=num2str(m);
    inputs.FeedInputs.uuid=run.cages.cageGroups{m}.cages{1}.inputsId;
    q=[name '-' label '-NONE-CageGroup' k '.depomodinputsproperties'];
    inputs.toFile(q)
end

%% Add extra time outputs
%create output over final 90 days, every 3 hours (set in seconds)
label='1'; %label of run to change
run=project.solidsRuns.label(label);
run.setOutputTimesFromEnd(90, 3*3600); 

%% Add aggregate surfaces
%create avg,min,max files for surface every 3 hours (set in days) between days 275 and 365
label='1'; %label of run to change
run=project.solidsRuns.label(label);
run.setAggregateTimes(275,365,0.125);

%% Change model parameters
run.physicalPropertiesFile
% any paramter that is shown in the structure shown above can be changed -
% all parameters must be strings, even numeric.
run.physicalPropertiesFile.Transports.BedModel.tauECritMin = num2str(0.025);
run.physicalPropertiesFile.toFile
%% Check for consistency, and check SEPA defaults are set
checkConsist(project,run)


