%% Convert xyz bathy to csv bathy
addpath(genpath('C:\toolboxChanges\depomod-matlab-toolbox'));
name='bathyTEST';
projectDir = ['C:\DepomodProjects\' name];
project=NewDepomod.Project.create(projectDir);
filename=fopen([projectDir '\depomod\bathymetry\' name '.depomodbathymetryproperties']);
D = (textscan(filename, '%f%f%f','Delimiter',',', 'headerlines',11 ));
bathyAll=cell2mat(D);
GridSize=25;
[bathyX,bathyY] = ndgrid(min(bathyAll(:,1))+(GridSize/2):GridSize:max(bathyAll(:,1))-(GridSize/2),...
    min(bathyAll(:,2))+(GridSize/2):GridSize:max(bathyAll(:,2))-(GridSize/2));
bathyZ = griddata(bathyAll(:,1),bathyAll(:,2),bathyAll(:,3),bathyX,bathyY);
bathymetry=flip(bathyZ');

% replace xyz data with bathymetry grid, comma deliminated
% replace Domain.data.elementFormat= xyzGrid with Domain.data.elementFormat=csvRegularGrid
% reduce Domain.data.numberOfElementsX (and Y) by 1 - if it's set to 81 change to 80