%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Project setup                                     %                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add required toolboxes
addpath(genpath('C:\toolboxChanges\depomod-matlab-toolbox'));
addpath(genpath('C:\Matlab_Toolbox\os_toolbox-master'));

%% Read existing project
name = 'TestPROJECT';
projectDir = ['C:\DepomodProjects\' name];
project = NewDepomod.Project.create(projectDir);
inputsDir = [projectDir '\depomod\inputs\'];
resultsDir = [projectDir '\depomod\results\'];
% Create output directory to save images 
outputDir = [projectDir '\outputs'];
if ~exist(outputDir)
    mkdir(outputDir);
end
cd(outputDir)

%% Optimize biomass 
% find approxiamte biomass that will pass, initial biomass must be set high enough to ensure a fail
% change sur if arregrate not enabled
 
Nmax = 0.01; %code stops when change in biomass is less than 1%
meanMAX = 2000; %maximum allowed intensity
szeMAX = 1.05; %maximum allowed % of mixing zone

label='1'; %label of run that fails
run=project.solidsRuns.label(label);
inputs = run.inputsFile;
g=run.cages.size; %number of CageGroups
B = str2num(inputs.FeedInputs.biomass); %initialbiomass
A = 0;
C = B;
optRUN={};
n=1;

while (A/B)<(1-Nmax)
    if n>1
        C=(A+B)/2; %newbiomass 
        newlabel=[label '_optRun_' num2str(n-1)];
        run=project.solidsRuns.new('label',newlabel,'template',label);
        cd (inputsDir);
        for m=1:g
            k=num2str(m);
            p=[name '-' label '-NONE-CageGroup' k '.depomodinputsproperties'];
            q=[name '-' newlabel '-NONE-CageGroup' k '.depomodinputsproperties'];
            copyfile(p, q)
        end

        inputs=run.inputsFile;
        inputs.setBiomass(C)
        inputs.toFile;
        cd(inputsDir)
        inputs.data=inputs.data/g;
        inputs.FeedInputs.biomass=num2str(C/g);
        for m=1:g
            k=num2str(m);
            inputs.FeedInputs.uuid=run.cages.cageGroups{m}.cages{1}.inputsId;
            q=[name '-' newlabel '-NONE-CageGroup' k '.depomodinputsproperties'];
            inputs.toFile(q)
        end

    end
    run.execute('runInBackground',0)
    
    sur=Depomod.Sur.Solids.fromFile([resultsDir name '-' run.label '-NONE-N-solids-g0-avg.depomodresultssur'], 'version', 2);
    %sur = run.solidsSur;
    [MZsze,MZmean]=passFail(run,szeMAX,meanMAX);
    
    cd(outputDir);
    run.plot('sur', sur);
    text(0.97,0.8,['250 g mean = ' sprintf('%.0f',MZmean) ' g'],'Color','red','Units','normalized','HorizontalAlignment','right')
    text(0.97,0.75,['250g/mixZone area = ' sprintf('%.3f',MZsze)],'Color','red','Units','normalized','HorizontalAlignment','right')
    saveas(gca,sprintf('%s_Plot.png',run.label));
    
    rdMZsze = round(MZsze,3,'significant');
    rdMZmean = round(MZmean,4,'significant');
    
    optRUN(n,:)={run.label inputs.FeedInputs.stockingDensity C rdMZsze rdMZmean}
    
    if MZsze <= szeMAX && MZmean<=meanMAX;
       A=C;
    else
       B=C;
    end
    n=n+1;
end
optResults=cell2table(optRUN,'VariableNames',{'Run' 'stockingDensity' 'biomass' 'area' 'areamean'})
writetable(optResults)

%% run project until biomass passes 95%  
% first run should just fail
LIMIT =20; % max # to run of each biomass
PASSRATE=0.95; % pass rate
totalLimit=50; % stop after this many runs - even if required pass rate not achieved

SZPASS=1.05; %acceptable pass size for 250 contour
MNPASS=2000; %acceptable pass level for mean across 250 contour

label='2'; %label of run that fails
run=project.solidsRuns.label(label);
g=run.cages.size;
inputs=run.inputsFile;
allCageID=inputs.FeedInputs.uuid;
RUN={};
pass=0; fail=0; i=1;
while (pass+fail<LIMIT && i<=totalLimit);
    project = NewDepomod.Project.create(projectDir);
    run=project.solidsRuns.label(label);
    run.execute('runInBackground',0)
    sur=Depomod.Sur.Solids.fromFile([resultsDir name '-' run.label '-NONE-N-solids-g0-avg.depomodresultssur'], 'version', 2);
    %sur = run.solidsSur;
    [MZsze,MZmean]=passFail(run,1.05,2000);
    cd(outputDir)
    run.plot('sur', sur);
    text(0.97,0.8,['250 g mean = ' sprintf('%.0f',MZmean) ' g'],'Color','red','Units','normalized','HorizontalAlignment','right')
    text(0.97,0.75,['250g/mixZone area = ' sprintf('%.3f',MZsze)],'Color','red','Units','normalized','HorizontalAlignment','right')
    saveas(gca,sprintf('maxBiomass_Plot%d.png',i));
    biomass=str2num(inputs.FeedInputs.biomass);
    if MZsze<SZPASS && MZmean<MNPASS;
       RUN(i,:)={'pass' i pass+1 inputs.FeedInputs.stockingDensity biomass round(MZsze,3,'significant') round(MZmean,4,'significant')}
       pass=pass+1;
    else
       RUN(i,:)={'fail' i fail+1 inputs.FeedInputs.stockingDensity biomass round(MZsze,3,'significant') round(MZmean,4,'significant')}
       if 1-(fail+1/LIMIT)>=PASSRATE;
           fail=fail+1;
       else
           pass=0; fail=0; 
           areaFail=SZPASS/MZsze; intFail=MNPASS/MZmean;
           newProp=min(areaFail,intFail);
           newbiomass=biomass*newProp;
           inputs.setBiomass(newbiomass)
           inputs.FeedInputs.uuid=allCageID;
           inputs.toFile;
           cd(inputsDir)
           inputs.data=inputs.data/g;
           inputs.FeedInputs.biomass=num2str(newbiomass/g);
           for m=1:g
               k=num2str(m);
               inputs.FeedInputs.uuid=run.cages.cageGroups{m}.cages{1}.inputsId;
               q=[name '-' label '-NONE-CageGroup' k '.depomodinputsproperties'];
               inputs.toFile(q)
           end
       end
    end
    i=i+1;
    clear project run
end
cd(outputDir)
maxResults=cell2table(RUN,'VariableNames',{'passOrFail' 'attempt' 'passNumber' 'stockingDensity' 'biomass' 'area' 'areamean'})
writetable(maxResults)

