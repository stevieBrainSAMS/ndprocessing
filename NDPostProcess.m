%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Project setup                                     %                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add required toolboxes
addpath(genpath('C:\toolboxChanges\depomod-matlab-toolbox'));
addpath(genpath('C:\Matlab_Toolbox\os_toolbox-master'));

%% Read existing project
name='defaultTEST';
projectDir = ['C:\DepomodProjects\' name];
project=NewDepomod.Project.create(projectDir);
resultsDir = [projectDir '\depomod\results\'];
    
%% Create output directory to save images 
outputDir = [projectDir '\outputs'];
if ~exist(outputDir)
    mkdir(outputDir);
end

cd(outputDir)
%% Plot bathymetry 
BF=project.bathymetry.plot('contour',1)
xlabel('Easting (m)')
ylabel('Northing (m)')
title(name)
%saveas(BF,[name '_bathymetry.png'])

%%                                                                       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      Investigate existing results                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Find existing runs for project
findRuns(project)

%% Choose run
run=project.solidsRuns.number(1);
%run=project.solidsRuns.first
%run.project.solidsRuns.label('1')
%run.project.EmBZRuns.first
%run.project.TFBZRuns.first

%% Plot cages and save images
CP=run.plot('impact',0);
saveas(CP,[name '_' run.label '_cages.png'])

% add Mixing Zone
[x,y] = run.cages.cagePerimeter;
hold on
plot(x,y,'w')
saveas(CP,[name '_' run.label '_cages_withMixingZone.png'])


%% Plot final deposition
FDP=run.plot('impactContour',1);
saveas(FDP,[name '_' run.label 'finalDeposit.png'])

%% Plot average deposition and save image
sur=Depomod.Sur.Solids.fromFile([resultsDir name '-' run.label '-NONE-N-solids-g0-avg.depomodresultssur'], 'version', 2);
IP=run.plot('sur',sur,'impactContour',1);
saveas(IP,[name '_' run.label '_avgDeposit.png'])

%% Mixing zone as percentage of allowed size, overall intensity pass/fail
sur=Depomod.Sur.Solids.fromFile([resultsDir name '-' run.label '-NONE-N-solids-g0-avg.depomodresultssur'], 'version', 2);
%sur = run.solidsSur;

passFail(run,1.05,2000); % run, surface, allowed area of mixing zone (105%), allowed intensity (2000g)
% project & run must be reloaded if run has been repeated
%% Make video from multiple output times
surs=run.sur('type', 'solids', 'g', 0, 't', t);
cd(outputDir)
times=run.outputTimestamps;
writerObj = VideoWriter('Deposition.avi');
writerObj.FrameRate = 2; % set the seconds per image
open(writerObj);
for i=1:length(surs)
    sur=surs{i};
    run.plot('sur', sur); %plot 1, 250 and 1000g/m^2/yr contours
    title([name label ' - Day ' num2str(floor(times(i)/(24*60*60))) ' hr ' num2str(mod(times(i),24*60*60)/(60*60))]);
    frame=getframe(gcf);
    writeVideo(writerObj, frame);
    close(gcf)
end
close(writerObj);